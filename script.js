/*
Теоретичні питання.
1. Прототипом об'єкта X називається об'єкт Y , властивості та методи якого доступні об'єкту X як власні.
Тобто, якщо об'єкту Y призначаються якісь властивості, наприклад функція, яка щось вираховує і повертає результат, то
 всі екземпляри об'єкта X зможуть використовувати цю функцію.
 2. У конструкторі  класу-нащадка ключове слово super() використовується як функція, що викликає батьківський конструктор.
 Її необхідно викликати до першого звернення до ключового слова this в самому конструкторі.
 */
"use strict"
class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    getName(){
        return this.name;
    }
    getAge(){
        return this.age;
    }
    getSalary(){
        return this.salary;
    }
    set name(newName){
        this._name = newName;
    }
    set age(newAge){
        this._age = newAge;
    }
    set salary(newSalary){
        this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
        this.salary = salary * 3;
    }

    getLang() {
        return this.lang
    }
    getSalary() {
        return  this.salary;
    }
}
let employee1 = new Programmer("Valeriy Salenko", 60, 28000, "Ukrainian, Russian, English");
let employee2 = new Programmer("Aleksandr Brovko", 34, 22000, "Ukrainian, German");
let employee3 = new Programmer("Irina Mikhaylova", 27, 20000, "Ukrainian, English");
let employee4 = new Programmer("Luciano Roberts", 45,  30000,"English, German, Spanish");
console.log(employee1,employee2, employee3, employee4);